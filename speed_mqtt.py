import os
import re
import subprocess
import time
import paho.mqtt.client as mqtt
import json

mqtt_id = subprocess.check_output("hostname",stderr=subprocess.STDOUT,shell=True)
mqtt_id = mqtt_id.rstrip("\n")
print 'mqtt topic is :'+mqtt_id
mqtt_id =mqtt_id + '/network'

def measure_temp():
        temp = os.popen("vcgencmd measure_temp").readline()
        return (temp.replace("temp=",""))

# Create client instance and connect to in-Terra mqtt server
client = mqtt.Client()
client.connect("130.255.185.100",1883,60)
boot_msg = json.dumps({"time":time.strftime('%m/%d/%y,%H:%M'),"script":"speed test on boot"});

client.publish(mqtt_id,boot_msg)


response = subprocess.Popen('speedtest-cli --simple', shell=True, stdout=subprocess.PIPE).stdout.read()

ping = re.findall('Ping:\s(.*?)\s', response, re.MULTILINE)
download = re.findall('Download:\s(.*?)\s', response, re.MULTILINE)
upload = re.findall('Upload:\s(.*?)\s', response, re.MULTILINE)

ping[0] = ping[0].replace(',', '.')
download[0] = download[0].replace(',', '.')
upload[0] = upload[0].replace(',', '.')

temp = measure_temp()
print temp
print '{},{},{},{},{}'.format(time.strftime('%m/%d/%y'), time.strftime('%H:%M'), ping[0], download[0], upload[0])

mqtt_msg = json.dumps({"temperature":temp,"time":time.strftime('%m/%d/%y,%H:%M'),"ping":ping[0]+'ms',"download":download[0]+'Mbps',"upload":upload[0]+'Mbps'});
client.publish(mqtt_id,mqtt_msg)

