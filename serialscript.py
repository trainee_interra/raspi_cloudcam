#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import serial
import subprocess
from datetime import datetime
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import json
from gpiozero import LED
import requests
import base64

#FONT ="/usr/share/fonts/truetype/freefont/FreeSans.ttf"
FONT ="/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf"


#MQTT Topic according to the hostname of device
mqtt_id = subprocess.check_output("hostname",stderr=subprocess.STDOUT,shell=True)
mqtt_id = mqtt_id.rstrip("\n")
print 'mqtt topic is :'+mqtt_id


#############  MQTT  SECTION  ##########
#MQTT parameters
Broker = "130.255.185.100"
sub_topic = mqtt_id+'/sub'
pub_topic = mqtt_id+'/pub'

# when connecting to mqtt do this;
def on_connect(client, userdata, flags, rc):
    print("Connected "+str(rc))
    client.subscribe(sub_topic)

# when receiving a mqtt message do this;
def on_message(client, userdata, msg):
    time.sleep(1)
    wrtdat = str(msg.payload)
    file = open("config.json","w") 
    file.write(wrtdat)
    file.close
    print "message is:"
    print(wrtdat)
    print(msg.topic)



def on_publish(mosq, obj, mid):
    print("mid: " + str(mid))


def getMAC(interface='wlan0'):
  # Return the MAC address of the specified interface
  try:
    str = open('/sys/class/net/%s/address' %interface).read()
  except:
    str = "00:00:00:00:00:00"
  str = str.replace(':','')
  return str[0:12]

mac_add = getMAC('wlan0')
print mac_add
url="https://testapi.terra3d.ch/v1/sensors/webcams/" + mac_add + "/photos/"
headers={'Authorization': 'K6NxE7Yo1tCg86zAmtZxtKTIJk1q8U47'}
print url

#On the led to confirm script is running 
led = LED(5)
led.on()
print "on the led"

try:
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(Broker, 1883, 60)
    client.loop_start()
    client.publish(pub_topic, "Camera Script Start")
except Exception as e:
    print(e)
###########################################


#function to shutdown the raspi
def shutdown_raspi():
    command = "/usr/bin/sudo /sbin/shutdown -h now"
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output = process.communicate()[0]
    print output

#function to click a picture from raspi cam
def camera():
    command = "/home/pi/raspi_cloudcam/upload.sh"
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output = process.communicate()[0]
    print output

#function to run linux process commands
def subprocess_run(command):
     subprocess.call(command,shell=True)

print "ready"

## Read configuration file
time.sleep(10)
with open('config.json') as read_file:
    dat = json.load(read_file)
try:
    rotate = dat["rotate"]
    shutdown = dat["shutdown"]
    logo = dat["logo"]
    uart = dat["uart"]
    camera_id = dat["camera_id"]
    project = dat["project"]
    bme = dat["bme"]
    batt = dat["batt"]
    fcolor = dat["fontcolor"]

    print  rotate
    print  shutdown
    print  logo
    print  uart
    print 'Camera_ID:' + camera_id
    print 'project:' + project
    print fcolor
    client.publish(pub_topic,json.dumps(dat))
except Exception as e:
    print(e)
    print 'ERROR: in reading config file '
    client.publish(pub_topic,"ERROR CONFIG FILE")

#if uart is enabled in cofiguration file
if(uart == 1):
    #Initialize uart to communicate with logger, timeout is limited to one second only
    ser = serial.Serial(
            port='/dev/ttyS0', 
            baudrate = 9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout = 5
    )

    #send an ack to logger that boot is done and ready to receive the data from logger
    ser.write('$A\n')
    print "send data"
    time.sleep(3)
    #read data from logger
    uart_data=ser.read(ser.in_waiting)
    print uart_data

    #split temperature,pressure,humidity and battery voltage
    try:
	    print "filter the data"
            temperature,humidity,pressure,battery = uart_data.split(",")
            client.publish(pub_topic, "UART:"+uart_data)
            print "got the data"
    except Exception as e:
            print(e)
   	    temperature='23.54C'
            humidity='45.253%'
            pressure='921.345Pa'
            battery='12.3V'
            client.publish(pub_topic, "uart_error")

    #remove decimal and add units

    temperature = temperature.split(".")
    temperature = temperature[0] + "C"

    humidity = humidity.split(".")
    humidity = humidity[0] + "%"

    pressure = pressure.split(".")
    pressure = pressure[0] + "hPa"
    
    battery = battery.split("V")
    battery = battery[0] + "V"
   

    print temperature
    print humidity
    print pressure
    print battery



#file name format with time information
time_file=datetime.now().strftime('%Y%m%d_%H_%M')

#date time format to write on image
time_photo = datetime.now().strftime('%d.%m.%Y_%H:%M')

#click photo with raspi cam
#subprocess_run("cd /home/pi/raspi_cloudcam")
subprocess_run("raspistill -o output.jpg -w 2592 -h 1944 -q 100 --exif")


#rotate the photo 
if(rotate==90):
    subprocess_run("convert -rotate "+'"90"'+" output.jpg output.jpg")
if(rotate==180):
    subprocess_run("convert -rotate "+'"180"'+" output.jpg output.jpg")
if(rotate==270):
    subprocess_run("convert -rotate "+'"270"'+" output.jpg output.jpg")


#add swiss railway logo on image
if(logo == 1):
    subprocess_run("convert output.jpg /home/pi/raspi_cloudcam/swiss_railway.png -gravity northeast -geometry +0+2 -composite output.jpg")
elif logo == 2:
    subprocess_run("convert output.jpg /home/pi/raspi_cloudcam/eberhard.png -gravity northeast -geometry +0+2 -composite output.jpg")

#add temperature, humidity and pressure logo on image
if(bme == 1):
    subprocess_run("convert output.jpg /home/pi/raspi_cloudcam/bottom.png -gravity southwest -geometry +0+2 -composite output.jpg")
    subprocess_run("convert output.jpg /home/pi/raspi_cloudcam/nobatt.png -gravity northwest -geometry +0+2 -composite output.jpg")
else:
    if(batt == 1):
        subprocess_run("convert output.jpg /home/pi/raspi_cloudcam/onlybatt.png -gravity northwest -geometry +0+2 -composite output.jpg")

#add time text on image
subprocess_run("convert " + '"output.jpg"' + " -font " + FONT + " -pointsize 72 -fill "+ fcolor +" -annotate +100+80 " + time_photo +' "output.jpg"')

#add cam name on image
subprocess_run("convert " + '"output.jpg"' + " -font " + FONT + " -pointsize 72 -fill "+ fcolor +" -annotate +100+330 " + camera_id +' "output.jpg"')

try:
    if(bme == 1):
        subprocess_run("convert "+'"output.jpg"' + " -font " + FONT + " -pointsize 72 -fill "+ fcolor +" -annotate +105+1525 " + temperature + ' "output.jpg"')
        subprocess_run("convert "+'"output.jpg"' + " -font " + FONT + " -pointsize 72 -fill "+ fcolor +" -annotate +105+1655 " + humidity + ' "output.jpg"')
        subprocess_run("convert "+'"output.jpg"' + " -font " + FONT + " -pointsize 72 -fill "+ fcolor +" -annotate +105+1770 " + pressure + ' "output.jpg"')
        subprocess_run("convert "+'"output.jpg"' + " -font " + FONT + " -pointsize 72 -fill "+ fcolor +" -annotate +105+1890 " + battery + ' "output.jpg"')
    else:
        if(batt == 1):
            subprocess_run("convert "+'"output.jpg"' + " -font " + FONT + " -pointsize 72 -fill "+ fcolor +" -annotate +100+450 " + battery + ' "output.jpg"')
except Exception as e:
    print(e)

#add project name on image 
subprocess_run("convert " + '"output.jpg"' + " -font " + FONT + " -pointsize 72 -fill "+ fcolor +" -annotate +100+200 " + project + " /home/pi/raspi_cloudcam/photo/" + time_file + '.jpg')

dropbox_path = project + '/' + camera_id + '/'
client.publish(pub_topic, "Upload Started to " + dropbox_path + " @" + time.strftime("%d-%m-%y_%H:%M:%S"))
#upload the final image on dropbox
r=0
try:
    subprocess_run("sudo dropbox_uploader upload /home/pi/raspi_cloudcam/photo/" + time_file + '.jpg ' + dropbox_path)
    f=open("/home/pi/raspi_cloudcam/photo/" + time_file + ".jpg", "rb")
    base64_img = "data:image/png;base64," + base64.b64encode(f.read())
    payload={'img': base64_img}
    r=requests.post(url, data=payload, headers=headers)
    print r
except Exception as e:
    print(e)

client.publish(pub_topic, "Upload Finished @"+time.strftime("%d-%m-%y_%H:%M:%S")+", MAC:"+ mac_add  )

#Acknowledge to logger that upload is done
if(uart == 1):
    ser.write('$B\n')


#remove all the photos from local storage
subprocess_run("sudo rm /home/pi/raspi_cloudcam/photo/*.jpg")
subprocess_run("sudo rm /home/pi/raspi_cloudcam/*.jpg")
#close the serial port
ser.close()


time.sleep(1)

#Shut down the raspi , see you on next image , BYE!
if(shutdown == 1):
    client.publish(pub_topic, "Shutdown enable")
    shutdown_raspi()
else:
    client.publish(pub_topic, "Shutdown disable")
print "done"




