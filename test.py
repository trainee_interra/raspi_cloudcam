import requests
import base64
import subprocess

camera_id = "itra_pcam_0009"
url = "https://api.terra3d.ch/v1/sensors/webcams/" + camera_id + "/photos/"
headers={'Authorization': 'K6NxE7Yo1tCg86zAmtZxtKTIJk1q8U47'}


#function to run linux process commands
def subprocess_run(command):
     subprocess.call(command,shell=True)

def upload_photo(path):
	f=open(path, "rb")
        base64_img = "data:image/png;base64," + base64.b64encode(f.read())
        payload={'img': base64_img}
        r=requests.post(url, data=payload, headers=headers)
        print(r.status_code) # just for check - if it's good it will be 201

subprocess_run("raspistill -o output.jpg -w 2592 -h 1944 -q 100 --exif")
path = "output.jpg"
upload_photo(path)

